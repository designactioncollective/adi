@extends('layouts.app')

@section('content')
@include('partials.page-header')
<?php $count = 0; ?>
<div class="container">
  <div class="row">
    @while (have_posts()) @php the_post() @endphp

    <?php
    $count++;
    if($count==1):
      echo'<div class="col-sm-12">'; ?>
        @include('partials.content-press1')
        <?php

        echo '</div>';
      else:
        echo '<div class="col-12 col-sm-6">';
        ?>
        @include('partials.content')
        <?php
        echo '</div>';
      endif;
      ?>

      @endwhile
    </div><!--end row -->
  </div>
  {!! get_the_posts_navigation() !!}
  @endsection
