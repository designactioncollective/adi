
<article @php post_class() @endphp>
  <?php if(get_field('media_contact')):
    echo '<div class="media-contact">'.get_field('media_contact').'</div>';
  endif;
  ?>

  <div class="date text-uppercase h5 pb-3">
    <?php
    if(get_field('event_dates')):
      echo get_field('event_date');
    else:
      the_time('F j, Y');
    endif;
    ?>
  </div>

  <?php if(has_post_thumbnail()) : ?>
      <a class="pb-4" href="<?php echo get_the_permalink(); ?>">
        <?php the_post_thumbnail('single_img'); ?>
      </a>
  <?php endif; ?>

  <h2 class="entry-title mt-5"><a href="{{ get_permalink() }}">{{ get_the_title() }}</a></h2>

  <div class="entry-summary">
  @php the_content() @endphp
  </div>
</article>

<div class="my-5 pt-5 px-3 w-100 d-block border border-primary border-right-0 border-top-0 border-left-0"></div>
