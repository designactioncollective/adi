<div class="past">
  <div class="container">
    <div class="row">
      <div class="col-12 text-center">
        <h2 class="">Past Issues</h2>
      </div>
    </div>
  <div class="row">
    <?php
    $latest_cpt = get_posts("post_type=issues&numberposts=1");
    $latest_pdt_id = $latest_cpt[0]->ID;

    $args = (array(
      'post_type' => 'issues',
      'posts_per_page' => 3,
      'post__not_in' => array($latest_pdt_id),
    ));

    $the_query = new WP_Query( $args ); ?>
    <?php if ( $the_query->have_posts() ) : ?>
      <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
          @include('partials.content-feed1')
      <?php endwhile; ?>
      <?php wp_reset_postdata(); ?>
    <?php endif; ?>
  </div>
</div>
</div><!-- end past-->
