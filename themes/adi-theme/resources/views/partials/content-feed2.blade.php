
<div class="col-4 feed-post">
  <article class="@php post_class() @endphp">
  <?php
  if(has_post_thumbnail()) : ?>
                <div class="">
                    <a class="img-fluid" href="<?php echo get_the_permalink(); ?>">
                    <?php the_post_thumbnail(); ?>
                  </a>
                </div>
  <?php endif; ?>
@include('partials.inserts._terms')
    <h2 class="entry-title"><a href="{{ get_permalink() }}">{!! get_the_title() !!}</a></h2>
</article><!-- end feed post-->
</div>
