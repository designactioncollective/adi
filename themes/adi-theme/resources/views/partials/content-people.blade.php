<?php
$position = get_field('job-title');
$affiliation = get_field('affiliation');
?>

<div class="col-12 col-sm-4 card border-0 my-4">
  <article @php post_class() @endphp>
    <div>

      <h4 class="entry-title h5 text-body neusaNextPro-medium">
        {{ get_the_title() }}
      </h4>

      <?php
      if(has_term('staff', 'role') || has_term('board', 'role')):
        if($position): echo '<span class="text-muted h6">'.$position.'</span>'; endif;
      endif;
      if(has_term('board', 'role')):
        if($affiliation): echo '<span class="sep text-muted">/</span><span class="text-muted h6">'.$affiliation.'</span>'; endif;
      endif;
      if(has_term('trustee', 'role')):
        if($affiliation): echo '<span class="text-muted h6">'.$affiliation.'</span>'; endif;
      endif; ?>

    </div>
  </article>
</div>
