@extends('layouts.app')

@section('content')
@include('partials.page-header')
<?php $count = 0; ?>
<div class="container">
  <div class="row">
    @while (have_posts()) @php the_post() @endphp

    <?php
    $count++;
    if($count==1):
      echo'<div class="col-sm-12 card-first">'; ?>
        @include('partials.content-'.get_post_type())
        <?php
        echo '</div><div class="my-5 mx-3 w-100 d-block border border-primary border-right-0 border-top-0 border-left-0"></div>';
      else:
        echo '<div class="col-12 col-sm-6">';
        ?>
        @include('partials.content-'.get_post_type())
        <?php
        echo '</div>';
      endif;
      ?>

      @endwhile
    </div><!--end row -->
  </div>
  {!! get_the_posts_navigation() !!}
  @endsection
