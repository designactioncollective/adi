
<?php if (is_single()&& get_post_type() == 'articles'):?>
<!--  <time class="date text-uppercase h5 pb-3"> <?php //the_time('F j, Y');?></time>-->

  <?php
						$authors = get_field('press_author');
						?>
						<?php if( $authors ): ?>
							<ul>
							<?php foreach( $authors as $author ): ?>
								<li>
									<a href="<?php echo get_permalink( $author->ID ); ?>">
										<?php echo get_the_title( $author->ID ); ?>
									</a>
								</li>
							<?php endforeach; ?>
							</ul>
						<?php endif; ?>



<?php
          $issues = get_field('which_issue');
          ?>
          <?php if( $issues ): ?>
            <ul>
            <?php foreach( $issues as $issue ): ?>
              <li>
                <a href="<?php echo get_permalink( $issue->ID ); ?>">
                  <?php echo get_field('issue_number', $issue->ID ); ?>
                </a>
              </li>
            <?php endforeach; ?>
            </ul>
          <?php endif; ?>

<?php endif; ?>

<?php if (is_single()&& get_post_type() == 'issues'):
echo get_field('issue_number');
endif;
?>




{{--{{
  <p class="byline author vcard">
    {{ __('', 'sage') }} <a href="{{ get_author_posts_url(get_the_author_meta('ID')) }}" rel="author" class="fn">
    </a>
  </p>
}}--}}
