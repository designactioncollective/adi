
<div class="col-4 feed-post">
  <article @php post_class() @endphp  >
  <?php
  if(has_post_thumbnail()) : ?>
                <div class="">
                    <a class="img-fluid" href="<?php echo get_the_permalink(); ?>">
                    <?php the_post_thumbnail(); ?>
                  </a>
                </div>
  <?php endif; ?>
  <?php if(get_post_type() != 'issues'){ ?>
@include('partials.inserts._article_cats')
<?php } ?>
    <h2 class="entry-title"><a href="{{ get_permalink() }}">{!! get_the_title() !!}</a></h2>

  <p><?php the_advanced_excerpt();?></p>
  @include('partials.inserts._terms')
</article><!-- end feed post-->
</div>
