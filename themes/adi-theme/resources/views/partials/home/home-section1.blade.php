
<?php
$args = array(
	'post_type' => 'issues',
	'posts_per_page' => 1
);
$the_query = new WP_Query( $args );
 if ( $the_query->have_posts() ) :?>
 <section class="home-1">
	 <div class="container">
	 <div class="row">
	 <?php
	 while ( $the_query->have_posts() ) : $the_query->the_post();
	$issue_num = get_field('issue_number');
	?>
	<div class="col">
		<?php if(has_post_thumbnail()) : ?>
									<div class="embed-responsive embed-responsive-24by49 fadeIn-trigger">
										<a class="image-fit_wrap fadeInLarge" href="<?php echo get_the_permalink(); ?>">
											<?php the_post_thumbnail(); ?>
										</a>
									</div>
		<?php endif; ?>

		<h2><?php the_title(); ?></h2>
		<span><?php echo $issue_num; ?></span>
		<p><?php the_advanced_excerpt(); ?></p>
	</div>
	<?php endwhile; ?>
	<?php wp_reset_postdata(); ?>
<?php endif; ?>
</div>
</div>
</section>
