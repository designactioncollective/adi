<?php

//https://wordpress.stackexchange.com/questions/150274/check-to-see-if-more-than-one-post
 $check_issue = new WP_Query( array(
  'post_type' => 'issues',
) );

$the_count = $check_issue->found_posts;
if ( ( $check_issue->have_posts() ) && ( $the_count > 1 ) ) :

$owlclass = 'owl-carousel';
$hascol = 'normal';
$postnum = 3;


else:

$owlclass = 'normal';
$postnum = 12;
$hascol = 'col-4';
endif;

$latest_cpt = get_posts("post_type=issues&numberposts=1");
$this_issue = $latest_cpt[0]->ID;
$is_name = $latest_cpt[0]->post_title;

		$args = (array(
			'post_type' => 'articles',
			'posts_per_page' => $postnum,
			'meta_query' => array(
										array(
											'key' => 'which_issue', // name of custom field
											'value' => '"' . $this_issue . '"', // matches exactly "123", not just 123. This prevents a match for "1234"
											'compare' => 'LIKE'
										)
									)
		)); ?>
		<section class="home-2">
			<div class="container">
			<div class="row">
				<div class="col"><h2>In this Issue</h2></div>
			</div>
			<div class="row <?php echo $owlclass; ?>">
			<?php
		$the_query2 = new WP_Query( $args ); ?>
		<?php if ( $the_query2->have_posts() ) : ?>
      
			<?php while ( $the_query2->have_posts() ) : $the_query2->the_post(); ?>

        <div class="<?php echo $hascol; ?> feed-post">
          <article @php post_class() @endphp  >
          <?php
          if(has_post_thumbnail()) : ?>
                        <div class="">
                            <a class="img-fluid" href="<?php echo get_the_permalink(); ?>">
                            <?php the_post_thumbnail(); ?>
                          </a>
                        </div>
          <?php endif; ?>
          <?php if(get_post_type() != 'issues'){ ?>
        @include('partials.inserts._terms')
        <?php } ?>
            <h2 class="entry-title"><a href="{{ get_permalink() }}">{!! get_the_title() !!}</a></h2>

          <p><?php the_advanced_excerpt();?></p>
        </article><!-- end feed post-->
        </div>
			<?php endwhile; ?>

			<?php wp_reset_postdata(); ?>
		<?php endif; ?>
</div>
</div>
</section>
