<?php


//https://wordpress.stackexchange.com/questions/150274/check-to-see-if-more-than-one-post
 $check_issue = new WP_Query( array(
  'post_type' => 'issues',
) );

$the_count = $check_issue->found_posts;
if ( ( $check_issue->have_posts() ) && ( $the_count > 1 ) ) :


						$args = (array(
							'post_type' => 'articles',
							'posts_per_page' => 12,
							'meta_query' => array(
														array(
															'key' => 'featured', // name of custom field
															'value' => 1,
															'compare' => 'LIKE'
														)
													)
						)); ?>
						<section class="home-3">
							<div class="container">
							<div class="row">
								<div class="col"><h2>Spotlight</h2></div>
							</div>
							<div class="row">
							<?php
						$the_query2 = new WP_Query( $args ); ?>
						<?php if ( $the_query2->have_posts() ) : ?>
							<?php while ( $the_query2->have_posts() ) : $the_query2->the_post(); ?>
									@include('partials.content-feed1')
							<?php endwhile; ?>
							<?php wp_reset_postdata(); ?>
						<?php endif; ?>
						</div>
						</div>
						</section>
<?php endif; ?>
