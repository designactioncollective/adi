
<article @php post_class() @endphp>
  <header class="page-header bg-primary text-white py-2 py-sm-4 mb-5">
    <div class="container py-2">
      <h1 class="entry-title mb-0">{{ get_the_title() }}</h1>
      @include('partials.inserts._share')
    </div>
  </header>
  <div class="container">
    <div class="entry-content">
      <h5 class="h5 text-uppercase"> @include('partials/entry-meta') </h5>
      <?php
      if(has_post_thumbnail()) : ?>
                    <div class="">
                        <a class="img-fluid" href="<?php echo get_the_permalink(); ?>">
                        <?php the_post_thumbnail(); ?>
                      </a>
                    </div>
      <?php endif; ?>
      @php the_content() @endphp

    </div>
  </div>
</article>
