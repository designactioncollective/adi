
<article @php post_class() @endphp>
  <header class="page-header bg-primary text-white py-2 py-sm-4 mb-5">
    <div class="container py-2">
      <h1 class="entry-title mb-0">{{ get_the_title() }}</h1>
      @include('partials.inserts._share')
    </div>
  </header>
  <div class="container">
    <div class="entry-content">
      <h5 class="h5 text-uppercase"> @include('partials/entry-meta') </h5>
      <div class="row">
      <div class="col-8 ">
        <?php
        if(has_post_thumbnail()) : ?>
                      <div class="">
                          <a class="img-fluid" href="<?php echo get_the_permalink(); ?>">
                          <?php the_post_thumbnail(); ?>
                        </a>
                      </div>
        <?php endif; ?>
      @php the_content() @endphp
      </div>

      <?php
      $args = (array(
        'post_type' => 'articles',
        'meta_query' => array(
                      array(
                        'key' => 'which_issue', // name of custom field
                        'value' => '"' . $post->ID. '"', // matches exactly "123", not just 123. This prevents a match for "1234"
                        'compare' => 'LIKE'
                      )
                    )
      ));

      $the_query = new WP_Query( $args ); ?>
      <?php if ( $the_query->have_posts() ) : ?>
        <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
            @include('partials.content-feed2')
        <?php endwhile; ?>
        <?php wp_reset_postdata(); ?>
      <?php endif; ?>


    </div><!-- end row -->
    </div>
  </div>
  @include('partials.past-issues')

</article>
