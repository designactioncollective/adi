<footer class="content-info">
  <div class="container">

  <div class="row">
    <div class="col text-center mb-5">
      <h2 class="">
        <?php
        if(get_field('footer_blurb', 291)):
          echo get_field('footer_blurb', 291);
        endif;
        $link_text = get_field('footer_link_text', 291);
        $link = get_field('footer_link', 291);
        ?>
      </h2>
        <?php if(get_field('footer_link_text', 291)):
        echo '<a class="" href="'.$link.'">'.$link_text.'</a>';
          endif;
      ?>
    </div>
  </div>
  <div class="row">
    <div class="col-12 col-sm-4 mb-3 mb-sm-0">
      <div class="fb_widget">
      <div class="fb-page" data-href="https://www.facebook.com/DesignActionCollective/" data-tabs="timeline" data-width="400" data-height="275" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/DesignActionCollective/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/DesignActionCollective/">Design Action Collective</a></blockquote></div>
    </div>
    </div>
    <div class="col-12 col-sm-4">
<a class="twitter-timeline" data-height="275" href="https://twitter.com/DesignAction?ref_src=twsrc%5Etfw">Tweets by DesignAction</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>    </div>
    <div class="col-12 col-sm-4">
        @php dynamic_sidebar('sidebar-footer') @endphp
    </div>
  </div>
  <div class="row">
    <div class="col">
    <a class="brand" href="{{ home_url('/') }}">{{ get_bloginfo('name', 'display') }}</a>
    <nav class="nav-footer">
      @if (has_nav_menu('primary_navigation'))
        {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']) !!}
      @endif
    </nav>
  </div><!--end menu col-->
  <div class="col">
    @include('partials/inserts._social')
    {!! wp_nav_menu(['menu' => 36, 'menu_class' => 'nav', 'depth' => 1]) !!}
  </div>

</div><!--end row-->
</div>
</footer>
