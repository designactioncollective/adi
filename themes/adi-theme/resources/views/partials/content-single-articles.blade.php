
<article @php post_class() @endphp>
  <header class="page-header bg-primary text-white py-2 py-sm-4 mb-5">
    <div class="container py-2">
      <h1 class="entry-title mb-0">{{ get_the_title() }}</h1>
      @include('partials.inserts._share')
    </div>
  </header>
  <div class="container">
    <div class="entry-content">
      <h5 class="h5 text-uppercase"> @include('partials.entry-meta') </h5>
      <?php
      if(has_post_thumbnail()) : ?>
                    <div class="">
                        <a class="img-fluid" href="<?php echo get_the_permalink(); ?>">
                        <?php the_post_thumbnail(); ?>
                      </a>
                    </div>
      <?php endif; ?>
      @include('partials.inserts._terms')
      @php the_content() @endphp
      @include('partials.inserts._article_tags')
    </div>
  </div>

  <!----- author bio ---->

  <div class="author">
    <div class="container">
    <div class="row">
      <div class="col text-center"><h2>Author</h2></div>
    </div>
    <div class="row">
  <?php
    $authors = get_field('press_author');
//var_dump $authors;
               foreach( $authors as $author ):
                    $this_author = $author->ID;
                    $this_title = $author->post_title;
                    $this_link = $author->post_link;
                  echo '<a href="'.$this_link.'">'.$this_title.'</a>';
                 endforeach;
?>


               </div>

</div>
</div>




<!--- also in this issue -->


  <div class="also">

<?php
$issues = get_field('which_issue');

           foreach( $issues as $issue ):
                $this_issue = $issue->ID;
             endforeach;

$args = (array(
  'post_type' => 'articles',
  'posts_per_page' => 3,
  'meta_query' => array(
                array(
                  'key' => 'which_issue', // name of custom field
                  'value' => '"' . $this_issue . '"', // matches exactly "123", not just 123. This prevents a match for "1234"
                  'compare' => 'LIKE'
                )
              )
)); ?>
  <div class="container">
  <div class="row">
    <div class="col text-center"><h2>Also in this Issue</h2></div>
  </div>
  <div class="row">
  <?php
$the_query2 = new WP_Query( $args ); ?>
<?php if ( $the_query2->have_posts() ) : ?>
  <?php while ( $the_query2->have_posts() ) : $the_query2->the_post(); ?>
      @include('partials.content-feed1')
  <?php endwhile; ?>
  <?php wp_reset_postdata(); ?>
<?php endif; ?>
  </div>
</div>
</div>
</article>
