<header class="banner">
  <div class="container">
    <div class="row">
      <div class="col-12 text-center">
        <h1><a class="brand" href="{{ home_url('/') }}">{{ get_bloginfo('name', 'display') }}</a></h1>
      </div>
      <div class="col-12 text-center mb-5">
        <?php if(get_field('tagline', 291)){
          echo get_field('tagline', 291);
        }
        ?>
      </div>
    </div>
    <div class="row">
      <div class="col">
        <nav class="nav-primary">
          @if (has_nav_menu('primary_navigation'))
            {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'slimmenu']) !!}
          @endif
        </nav>
      </div>
      <div class="col">
        @include('partials/inserts._social')
      </div>
      <div class="col">
        @include('partials/inserts._form')
      </div>
  </div>
</header>
