<div class="terms d-none d-md-block fontSize-8 mt-2">

	<?php
	$id = get_the_ID();
	if(get_the_term_list($id, 'article-type')):
		echo '<ul class="art-cat">'.get_the_term_list($id, 'article-type','<li class="">',  ', ', '</li>').'</ul>';
	endif;

	?>
</div>
