<div class="terms d-none d-md-block fontSize-8 mt-2">
	<?php
	$id = get_the_ID();
		if(get_the_term_list($id, 'article-tags')):
		echo '<ul class="art-cats">'.get_the_term_list($id, 'article-tags','<li class="">',  ', ', '</li>').'</ul>';
	endif;
	?>
</div>
