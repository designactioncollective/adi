<div class="bg-secondary py-2 py-sm-4 mb-5">
  <div class="container py-2 d-flex align-items-center justify-content-start">
    <h5 class="text-uppercase neusaNextPro-light mb-0 pr-2">Share this</h5>
    <div class="pl-2">
      <a class="" href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fadimagazine.com&quote=Adi%20Magazine" title="Share on Facebook" target="_blank">
        <i class="fab fa-facebook"></i>
        <span class="sr-only">Share on Facebook</span>
      </a>
    </div>
    <div class="pl-2">
      <a href="https://twitter.com/intent/tweet?source=http%3A%2F%2Fadimagazine.com&text=Adi%20Magazine:%20http%3A%2F%2Fadimagazine.com&via=Adi_Magazine" target="_blank" title="Tweet">
        <i class="fab fa-twitter"></i>
        <span class="sr-only">Tweet about it</span>
      </a>
    </div>
    <div class="pl-2">
      <a href="http://www.linkedin.com/shareArticle?mini=true&url=http%3A%2F%2Fadimagazine.com&title=Adi%20Magazine&summary=Adi%20magazine%20is%20rehumanizing%20policy.&source=http%3A%2F%2Fadimagazine.com" target="_blank" title="Share on LinkedIn">
        <i class="fab fa-linkedin-in"></i>
        <span class="sr-only">Share on LinkedIn</span>
      </a>
    </div>
    <div class="pl-2">
      <a href="mailto:?subject=Adi%20Magazine&body=Adi%20magazine%20is%20rehumanizing%20policy.:%20http%3A%2F%2Fadimagazine.com" target="_blank" title="Send email">
        <i class="fas fa-envelope"></i>
        <span class="sr-only">Send email</span>
      </a>
    </div>
  </div>
</div>
