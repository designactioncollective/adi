<ul class="social d-inline mb-0 pl-3">
	<li class="d-inline">
		<a class="text-body text-decoration-none facebook--hover" target="_blank" href="<?php echo get_theme_mod( 'facebook_link'); ?>">
			<i class="fab fa-facebook" aria-hidden="true"></i><span class="sr-only">facebook</span>
		</a>
	</li>
	<li class="d-inline">
		<a class="text-body text-decoration-none twitter--hover" target="_blank" href="<?php echo get_theme_mod( 'twitter_link'); ?>">
			<i class="fab fa-twitter ml-1" aria-hidden="true"></i><span class="sr-only">twitter</span>
		</a>
	</li>
	<li class="d-inline">
		<a class="text-body text-decoration-none" target="_blank" href="<?php  echo get_theme_mod( 'youtube_link'); ?>">
			<i class="fab fa-youtube" aria-hidden="true"></i><span class="sr-only">Youtube</span>
		</a>
	</li>
</ul>
