<!doctype html>
<html {!! get_language_attributes() !!}>
  @include('partials.head')
  <body @php body_class() @endphp>
    <div id="fb-root"></div>
      <script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.2';
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));</script>

    @php do_action('get_header') @endphp
    @include('partials.header')
    <?php if (is_front_page()): ?>
  <div class="wrap" role="document">
    <div class="content">
      @include('partials.home.home-section1')
      @include('partials.home.home-section2')
      @include('partials.home.home-section3')
    </div>
  </div>
<?php else: ?>
    <div class="wrap container-fluid px-0 mb-5" role="document">
      <div class="content">
        <main class="main">
          @yield('content')
        </main>
        @if (App\display_sidebar())
          <aside class="sidebar">
            @include('partials.sidebar')
          </aside>
        @endif
        @if(get_post_type() == 'articles')
        @include('partials.past-issues')
        @endif
      </div>
    </div>
<?php endif;  ?>
    @php do_action('get_footer') @endphp
    @include('partials.footer')
    @php wp_footer() @endphp
  </body>
</html>
