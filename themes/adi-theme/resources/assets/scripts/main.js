// import external dependencies
import 'jquery';

// import then needed Font Awesome functionality
import { library, dom } from '@fortawesome/fontawesome-svg-core';

// import the Facebook and Twitter icons
import { faFacebook, faTwitter, faYoutube, faGooglePlusG, faPinterestP, faLinkedinIn} from '@fortawesome/free-brands-svg-icons';

import { faHome, faAngleDown, faAngleUp, faEnvelope } from '@fortawesome/free-solid-svg-icons';

// add the imported icons to the library
library.add(faFacebook, faTwitter, faYoutube, faGooglePlusG, faPinterestP, faHome, faAngleDown, faAngleUp, faEnvelope, faLinkedinIn );

// tell FontAwesome to watch the DOM and add the SVGs when it detects icon markup
dom.watch();

// https://owlcarousel2.github.io/OwlCarousel2/docs/started-installation.html
$(document).ready(function(){
  $('.owl-carousel').owlCarousel({
	loop:true,
    responsiveClass:true,
    nav:true,
    navText : ['&lsaquo','&rsaquo;'],
    autoplay:true,
    responsive:{
        0:{
            items:1,

        },
        600:{
            items:3,

        },
        992:{
            items:3,
        },
    },
  });
});



// Import everything from autoload
import './autoload/**/*'

// import local dependencies
import Router from './util/Router';
import common from './routes/common';
import home from './routes/home';
import aboutUs from './routes/about';

/** Populate Router instance with DOM routes */
const routes = new Router({
  // All pages
  common,
  // Home page
  home,
  // About Us page, note the change from about-us to aboutUs.
  aboutUs,
});

// Load Events
jQuery(document).ready(() => routes.loadEvents());
