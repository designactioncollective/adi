<?php
// add social Links to customizer:
//
function merchandise_link_customizer( $wp_customize ) {
	//social networking
    $wp_customize->add_section( 'site_link_section' , array(
        'title'       => __( 'Site Links', 'site_links' ),
        'priority'    => 10,
        'description' => 'Enter links for social media icons',
    ) );

    $wp_customize->add_setting( 'facebook_link' );

    $wp_customize->add_control( 'facebook_link', array(
        'label'    => __( 'Facebook Link', 'site_links' ),
        'section'  => 'site_link_section',
        'type' => 'url',
        'settings' => 'facebook_link'
    ) );

     $wp_customize->add_setting( 'twitter_link' );

     $wp_customize->add_control( 'twitter_link', array(
        'label'    => __( 'Twitter Link', 'site_links' ),
        'section'  => 'site_link_section',
        'type' => 'url',
        'settings' => 'twitter_link'
    ) );
     $wp_customize->add_setting( 'youtube_link' );

     $wp_customize->add_control( 'youtube_link', array(
        'label'    => __( 'Youtube Link', 'site_links' ),
        'section'  => 'site_link_section',
        'type' => 'url',
        'settings' => 'youtube_link'
    ) );
    $wp_customize->add_setting( 'donate_link' );

    $wp_customize->add_control( 'donate_link', array(
       'label'    => __( 'Donate Link', 'site_links' ),
       'section'  => 'site_link_section',
       'type' => 'url',
       'settings' => 'donate_link'
   ) );
  // $wp_customize->add_setting( 'signup_link' );

  /* $wp_customize->add_control( 'signup_link', array(
      'label'    => __( 'Sign-up Link', 'site_links' ),
      'section'  => 'site_link_section',
      'type' => 'url',
      'settings' => 'signup_link'
  ) ); */

}
add_action('customize_register',  __NAMESPACE__ . '\\merchandise_link_customizer');
