<?php
/*
Plugin Name: Design Action - Sidebar Menus + Breadcrumbs
Description: Creates submenus (for use in sidebars) and breadcrumbs, with structure based on the primary menu.
Version:     0.0.2
Author:      Design Action
Author URI:  http://designaction.org
*/
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

// add hook
add_filter( 'wp_nav_menu_objects', 'my_wp_nav_menu_objects_sub_menu', 10, 2 );

// filter_hook function to react on sub_menu flag
function my_wp_nav_menu_objects_sub_menu( $sorted_menu_items, $args ) {
  global $post;

  if ( isset( $args->sub_menu ) ) {
    $root_id = 0;

    // find the current menu item
    foreach ( $sorted_menu_items as $menu_item ) {

      // If we're on a single of a custom post type, show the correct submenu
	  if ( isset($post) && ($menu_item->type == 'post_type_archive') && ($menu_item->object == $post->post_type) ) {        $menu_item->current = true;
      }
      // If single of taxonomy and term archive is in the menu, show the correct submenu {
      if ( isset($post) && ($menu_item->type == 'taxonomy') && has_term($menu_item->object_id, $menu_item->object, $post->ID) ) {
      	$menu_item->current = true;
      }

      if ( $menu_item->current ) {
        // set the root id based on whether the current menu item has a parent or not
        $root_id = ( $menu_item->menu_item_parent ) ? $menu_item->menu_item_parent : $menu_item->ID;
        break;
      }
    }


    /*
    * START CUSTOMIZATION
    * override menu id if 'menu_id' parameter is specified in shortcode
    *
    */
    if ($args->manual_menu_id != '') {
     $root_id = $args->manual_menu_id;
   }
    /*
    * END CUSTOMIZATION
    *
    */

    // find the top level parent
    if ( ! isset( $args->direct_parent ) ) {
      $prev_root_id = $root_id;
      while ( $prev_root_id != 0 ) {
        foreach ( $sorted_menu_items as $menu_item ) {
          if ( $menu_item->ID == $prev_root_id ) {
            $prev_root_id = $menu_item->menu_item_parent;
            // don't set the root_id to 0 if we've reached the top of the menu
            if ( $prev_root_id != 0 ) $root_id = $menu_item->menu_item_parent;
            break;
          }
        }
        break;

      }
    }

    $menu_item_parents = array();
    foreach ( $sorted_menu_items as $key => $item ) {
      // init menu_item_parents
      if ( $item->ID == $root_id ) $menu_item_parents[] = $item->ID;

      if ( in_array( $item->menu_item_parent, $menu_item_parents ) ) {
        // part of sub-tree: keep!
        $menu_item_parents[] = $item->ID;
      } else if ( ! ( isset( $args->show_parent ) && in_array( $item->ID, $menu_item_parents ) ) ) {
        // not part of sub-tree: away with it!
        unset( $sorted_menu_items[$key] );
      }
    }

    return $sorted_menu_items;
  } else {
    return $sorted_menu_items;
  }
}

function display_the_submenu($atts) {
  //set menu_id default
  $a = shortcode_atts( array(
          'menu_id' => ''
      ), $atts );

  return wp_nav_menu( array(
    'theme_location' => 'primary_navigation',
    'sub_menu' => true,
    'show_parent' => true,
    'ancestor_parent' => false,
    'container' => '',
    //'menu_id' => 'sidebar-menu',
    'menu_class' => 'sidebar-menu',
    'echo' => false,
    'manual_menu_id' => $a['menu_id']
  ) );
}
// Register Shortcodes
function register_shortcodes(){
   add_shortcode('submenu', 'display_the_submenu');
}
add_action( 'init', 'register_shortcodes');


// Custom Breadcrumb Walker
class DAC_Breadcrumbs extends Walker_Nav_Menu {
  public function start_lvl( &$output, $depth = 0, $args = array() ) {
    $output .= '';
  }

  public function end_lvl( &$output, $depth = 0, $args = array() ) {
    $output .= '';
  }

  public function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
    // Skip all items except for the current item and its ancestors
    if ($item->current || $item->current_item_parent || $item->current_item_ancestor) :

      // Setup item classes
      $item_classes  = 'breadcrumb-item';
      if ($item->current && !(is_singular() && !is_page())) $item_classes .= ' active text-white';

      // If current, add aria-current attribute
      $item_aria = $item->current ? ' aria-current="page"' : '';

      // Set up title
      $title = apply_filters( 'the_title', $item->title, $item->ID );

      // Link
      $item_a = '';
      $item_a_close = '';
      if (!$item->current || (is_singular() && !is_page())) {
        $item_a = '<a class="text-white" href="'.$item->url.'">';
        $item_a_close = '</a>';
      }

      // Structure the output
      $item_output  = '<li class="'.$item_classes.'" '.$item_aria.'>';
      $item_output .= $item_a;
      $item_output .= $title;
      $item_output .= $item_a_close;
      $item_output .= '</li>';

      $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );

    endif;
  }

  public function end_el( &$output, $item, $depth = 0, $args = array() ) {
    $output .= '';
  }
}

// Output the breadcrumbs
function dac_the_breadcrumbs($menu_location, $atts = '') {
  $home_link        = '<li class="breadcrumb-item"><a class="text-white" href="'.home_url().'">'.__('Home', 'dac').'</a></li>';

  // Wrapping tags
  $container_before = '<nav aria-label="breadcrumb"><ol class="breadcrumb bg-transparent p-0 mb-0 neusaNextPro-medium">';
  $container_after  = '</ol></nav>';
  $active_before    = '<li class="breadcrumb-item active text-white" aria-current="page">';
  $active_after     = '</li>';

  $single_link      = ( is_singular() && !is_page() ) ? $active_before.get_the_title().$active_after : '';

  // Search
  if (is_search()) {
    $search = sprintf(__('Search Results for %s', 'dac'), get_search_query());
    echo $container_before.$home_link.$active_before.$search.$active_after.$container_after;
  }

  // 404
  elseif (is_404()) {
    echo $container_before.$home_link.$active_before.'404'.$active_after.$container_after;
  }

  // Single tribe_events
  elseif (is_singular('tribe_events')) {
    // The Events Calendar cannot use get_the_title outside the loop, so we have to access the global query vars
    global $wp_query;

    $events_link = '<li class="breadcrumb-item"><a class="text-white" href="'.tribe_get_events_link().'">'.__('Calendar', 'dac').'</a></li>';
    echo $container_before.$home_link.$events_link.$active_before.get_the_title($wp_query->queried_object->ID).$active_after.$container_after;
  }


  // Tribe_events archive
  elseif (is_post_type_archive('tribe_events')) {
    $events_link = $active_before.__('Calendar', 'dac').$active_after;
    echo $container_before.$home_link.$events_link.$container_after;
  }

  // All other instances
  else {
    //set menu_id default
    $a = shortcode_atts( array(
            'menu_id' => ''
        ), $atts );

    wp_nav_menu( array(
      'theme_location'  => $menu_location,
      'sub_menu'        => true,
      'show_parent'     => true,
      'ancestor_parent' => false,
      'container'       => '',
      'items_wrap'      => $container_before.$home_link.'%3$s'.$single_link.$container_after,
      'item_spacing'    => 'discard',
      'walker'          => new DAC_Breadcrumbs,
      'manual_menu_id'  => $a['menu_id']
    ) );
  }
}
