<?php
/*
Plugin Name: DAC - Staff and Attorneys
Description: <strong>Staff and Attorneys</strong> Functionality
Version:     0.0.1
Author:      Design Action
Author URI:  http://designaction.org
*/
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );


// Register Custom Taxonomy
function custom_taxonomy_cat() {

	$labels = array(
		'name'                       => _x( 'Categories', 'Taxonomy General Name', 'adi' ),
		'singular_name'              => _x( 'Category', 'Taxonomy Singular Name', 'adi' ),
		'menu_name'                  => __( 'Categories', 'adi' ),
		'all_items'                  => __( 'All Categories', 'adi' ),
		'parent_item'                => __( 'Parent Category', 'adi' ),
		'parent_item_colon'          => __( 'Parent Category:', 'adi' ),
		'new_item_name'              => __( 'New Category Name', 'adi' ),
		'add_new_item'               => __( 'Add New Category', 'adi' ),
		'edit_item'                  => __( 'Edit Category', 'adi' ),
		'update_item'                => __( 'Update Category', 'adi' ),
		'view_item'                  => __( 'View Category', 'adi' ),
		'separate_items_with_commas' => __( 'Separate Category with commas', 'adi' ),
		'add_or_remove_items'        => __( 'Add or remove Category', 'adi' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'adi' ),
		'popular_items'              => __( 'Popular Categories', 'adi' ),
		'search_items'               => __( 'Search Categories', 'adi' ),
		'not_found'                  => __( 'Not Found', 'adi' ),
		'no_terms'                   => __( 'No Categories', 'adi' ),
		'items_list'                 => __( 'Categories list', 'adi' ),
		'items_list_navigation'      => __( 'Categories list navigation', 'adi' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
		'show_in_rest'          => true,
	);
	register_taxonomy( 'article-category', array( 'articles' ), $args );

}
add_action( 'init', 'custom_taxonomy_cat', 0 );
