<?php
/*
Plugin Name: DAC - Staff and Attorneys
Description: <strong>Staff and Attorneys</strong> Functionality
Version:     0.0.1
Author:      Design Action
Author URI:  http://designaction.org
*/
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

// Register Custom Post Type
function custom_post_type() {

	$labels = array(
		'name'                  => _x( 'Articles', 'Post Type General Name', 'adi' ),
		'singular_name'         => _x( 'Article', 'Post Type Singular Name', 'adi' ),
		'menu_name'             => __( 'Articles', 'adi' ),
		'name_admin_bar'        => __( 'Article', 'adi' ),
		'archives'              => __( 'Article Archives', 'adi' ),
		'attributes'            => __( 'Article Attributes', 'adi' ),
		'parent_item_colon'     => __( 'Parent Article:', 'adi' ),
		'all_items'             => __( 'All Articles', 'adi' ),
		'add_new_item'          => __( 'Add New Article', 'adi' ),
		'add_new'               => __( 'Add New', 'adi' ),
		'new_item'              => __( 'New Article', 'adi' ),
		'edit_item'             => __( 'Edit Article', 'adi' ),
		'update_item'           => __( 'Update Article', 'adi' ),
		'view_item'             => __( 'View Article', 'adi' ),
		'view_items'            => __( 'View Articles', 'adi' ),
		'search_items'          => __( 'Search Article', 'adi' ),
		'not_found'             => __( 'Not found', 'adi' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'adi' ),
		'featured_image'        => __( 'Featured Image', 'adi' ),
		'set_featured_image'    => __( 'Set featured image', 'adi' ),
		'remove_featured_image' => __( 'Remove featured image', 'adi' ),
		'use_featured_image'    => __( 'Use as featured image', 'adi' ),
		'insert_into_item'      => __( 'Insert into Article', 'adi' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'adi' ),
		'items_list'            => __( 'Articles list', 'adi' ),
		'items_list_navigation' => __( 'Articles list navigation', 'adi' ),
		'filter_items_list'     => __( 'Filter Articles list', 'adi' ),
	);
	$args = array(
		'label'                 => __( 'Article', 'adi' ),
		'description'           => __( 'Post Type Description', 'adi' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', 'revisions', 'custom-fields' ),
		'taxonomies'            => array( 'article-category', 'article-tags', 'article-type' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
		'show_in_rest'          => true,
	);
	register_post_type( 'articles', $args );

}
add_action( 'init', 'custom_post_type', 0 );
