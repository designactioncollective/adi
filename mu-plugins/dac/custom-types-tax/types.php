<?php
/*
Plugin Name: DAC - Staff and Attorneys
Description: <strong>Staff and Attorneys</strong> Functionality
Version:     0.0.1
Author:      Design Action
Author URI:  http://designaction.org
*/
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

// Register Custom Taxonomy
function custom_taxonomy() {

	$labels = array(
		'name'                       => _x( 'Types', 'Taxonomy General Name', 'adi' ),
		'singular_name'              => _x( 'Type', 'Taxonomy Singular Name', 'adi' ),
		'menu_name'                  => __( 'Type', 'adi' ),
		'all_items'                  => __( 'All Types', 'adi' ),
		'parent_item'                => __( 'Parent Type', 'adi' ),
		'parent_item_colon'          => __( 'Parent Type:', 'adi' ),
		'new_item_name'              => __( 'New Type Name', 'adi' ),
		'add_new_item'               => __( 'Add New Type', 'adi' ),
		'edit_item'                  => __( 'Edit Type', 'adi' ),
		'update_item'                => __( 'Update Type', 'adi' ),
		'view_item'                  => __( 'View Type', 'adi' ),
		'separate_items_with_commas' => __( 'Separate Types with commas', 'adi' ),
		'add_or_remove_items'        => __( 'Add or remove Types', 'adi' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'adi' ),
		'popular_items'              => __( 'Popular Types', 'adi' ),
		'search_items'               => __( 'Search Types', 'adi' ),
		'not_found'                  => __( 'Not Found', 'adi' ),
		'no_terms'                   => __( 'No Types', 'adi' ),
		'items_list'                 => __( 'Types list', 'adi' ),
		'items_list_navigation'      => __( 'Types list navigation', 'adi' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => false,
		'show_in_rest'               => true,
	);
	register_taxonomy( 'article-type', array( 'articles' ), $args );

}
add_action( 'init', 'custom_taxonomy', 0 );
