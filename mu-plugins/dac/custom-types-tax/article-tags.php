<?php
/*
Plugin Name: DAC - Staff and Attorneys
Description: <strong>Staff and Attorneys</strong> Functionality
Version:     0.0.1
Author:      Design Action
Author URI:  http://designaction.org
*/
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

// Register Custom Taxonomy
function custom_taxonomy_tags() {

	$labels = array(
		'name'                       => _x( 'Tags', 'Taxonomy General Name', 'adi' ),
		'singular_name'              => _x( 'Tag', 'Taxonomy Singular Name', 'adi' ),
		'menu_name'                  => __( 'Tag', 'adi' ),
		'all_items'                  => __( 'All Tags', 'adi' ),
		'parent_item'                => __( 'Parent Tag', 'adi' ),
		'parent_item_colon'          => __( 'Parent Tag:', 'adi' ),
		'new_item_name'              => __( 'New Tag Name', 'adi' ),
		'add_new_item'               => __( 'Add New Tag', 'adi' ),
		'edit_item'                  => __( 'Edit Tag', 'adi' ),
		'update_item'                => __( 'Update Tag', 'adi' ),
		'view_item'                  => __( 'View Tag', 'adi' ),
		'separate_items_with_commas' => __( 'Separate Tags with commas', 'adi' ),
		'add_or_remove_items'        => __( 'Add or remove Tags', 'adi' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'adi' ),
		'popular_items'              => __( 'Popular Tags', 'adi' ),
		'search_items'               => __( 'Search Tags', 'adi' ),
		'not_found'                  => __( 'Not Found', 'adi' ),
		'no_terms'                   => __( 'No Tags', 'adi' ),
		'items_list'                 => __( 'Tags list', 'adi' ),
		'items_list_navigation'      => __( 'Tags list navigation', 'adi' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => false,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
    'show_in_rest'          => true,
	);
	register_taxonomy( 'article-tags', array( 'articles' ), $args );

}
add_action( 'init', 'custom_taxonomy_tags', 0 );
