<?php
/*
Plugin Name: DAC - Staff and Attorneys
Description: <strong>Staff and Attorneys</strong> Functionality
Version:     0.0.1
Author:      Design Action
Author URI:  http://designaction.org
*/
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );


// Register Custom Post Type
function custom_post_type_press() {

	$labels = array(
		'name'                  => _x( 'Press Releases', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'Press Release', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'Press Releases', 'text_domain' ),
		'name_admin_bar'        => __( 'Press Release', 'text_domain' ),
		'archives'              => __( 'Item Archives', 'text_domain' ),
		'attributes'            => __( 'Event Attributes', 'text_domain' ),
		'parent_item_colon'     => __( 'Parent Press Release:', 'text_domain' ),
		'all_items'             => __( 'All Press Releases', 'text_domain' ),
		'add_new_item'          => __( 'Add New Press Release', 'text_domain' ),
		'add_new'               => __( 'Add New', 'text_domain' ),
		'new_item'              => __( 'New Press Release', 'text_domain' ),
		'edit_item'             => __( 'Edit Press Release', 'text_domain' ),
		'update_item'           => __( 'Update Press Release', 'text_domain' ),
		'view_item'             => __( 'View Press Release', 'text_domain' ),
		'view_items'            => __( 'View Press Releases', 'text_domain' ),
		'search_items'          => __( 'Search Press Release', 'text_domain' ),
		'not_found'             => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
		'featured_image'        => __( 'Featured Image', 'text_domain' ),
		'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
		'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
		'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
		'insert_into_item'      => __( 'Insert into Press Release', 'text_domain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
		'items_list'            => __( 'Items list', 'text_domain' ),
		'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
		'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
	);
	$args = array(
		'label'                 => __( 'Press Release', 'text_domain' ),
		'description'           => __( 'Post Type Description', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'revisions', 'custom-fields' ),
		'show_in_rest'					=> true,
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-megaphone',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'press-release', $args );

}
add_action( 'init', 'custom_post_type_press', 0 );
