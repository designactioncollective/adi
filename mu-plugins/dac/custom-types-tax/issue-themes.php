<?php
/*
Plugin Name: DAC - Staff and Attorneys
Description: <strong>Staff and Attorneys</strong> Functionality
Version:     0.0.1
Author:      Design Action
Author URI:  http://designaction.org
*/
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

// Register Custom Taxonomy
function custom_taxonomy_themes() {

	$labels = array(
		'name'                       => _x( 'Themes', 'Taxonomy General Name', 'text_domain' ),
		'singular_name'              => _x( 'Theme', 'Taxonomy Singular Name', 'text_domain' ),
		'menu_name'                  => __( 'Theme', 'text_domain' ),
		'all_items'                  => __( 'All Themes', 'text_domain' ),
		'parent_item'                => __( 'Parent Theme', 'text_domain' ),
		'parent_item_colon'          => __( 'Parent Theme:', 'text_domain' ),
		'new_item_name'              => __( 'New Theme Name', 'text_domain' ),
		'add_new_item'               => __( 'Add New Theme', 'text_domain' ),
		'edit_item'                  => __( 'Edit Theme', 'text_domain' ),
		'update_item'                => __( 'Update Theme', 'text_domain' ),
		'view_item'                  => __( 'View Theme', 'text_domain' ),
		'separate_items_with_commas' => __( 'Separate Theme with commas', 'text_domain' ),
		'add_or_remove_items'        => __( 'Add or remove Theme', 'text_domain' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'text_domain' ),
		'popular_items'              => __( 'Popular Themes', 'text_domain' ),
		'search_items'               => __( 'Search Themes', 'text_domain' ),
		'not_found'                  => __( 'Not Found', 'text_domain' ),
		'no_terms'                   => __( 'No Themes', 'text_domain' ),
		'items_list'                 => __( 'Theme list', 'text_domain' ),
		'items_list_navigation'      => __( 'Theme list navigation', 'text_domain' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => false,
    'show_in_rest'					     => true,
	);
	register_taxonomy( 'issue-themes', array( 'issues' ), $args );

}
add_action( 'init', 'custom_taxonomy_themes', 0 );
