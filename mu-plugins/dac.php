<?php
/**
 * Plugin Name: Design Action plugins
 * Description: A collection of must-use plugins
 * Author: Design Action Collective
*/


/*
 * Load all mu-plugins
*/

/* Functions */
require_once(dirname(__FILE__) . '/dac/functions/sidebar-menus.php');
require_once(dirname(__FILE__) . '/dac/functions/menu-shortcode.php');
require_once(dirname(__FILE__) . '/dac/functions/acf_share_options.php');
require_once(dirname(__FILE__) . '/dac/functions/pagination.php');
require_once(dirname(__FILE__) . '/dac/functions/site_links.php');

/* Custom post types and taxonomies */
require_once(dirname(__FILE__) . '/dac/custom-types-tax/dac-staff-board.php');
require_once(dirname(__FILE__) . '/dac/custom-types-tax/press.php');
require_once(dirname(__FILE__) . '/dac/custom-types-tax/issues.php');
require_once(dirname(__FILE__) . '/dac/custom-types-tax/articles.php');
require_once(dirname(__FILE__) . '/dac/custom-types-tax/article-categories.php');
require_once(dirname(__FILE__) . '/dac/custom-types-tax/article-tags.php');
require_once(dirname(__FILE__) . '/dac/custom-types-tax/article-authors.php');
require_once(dirname(__FILE__) . '/dac/custom-types-tax/types.php');
require_once(dirname(__FILE__) . '/dac/custom-types-tax/issue-themes.php');
